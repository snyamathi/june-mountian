package com.snyamathi.June;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MenuAdapter extends BaseAdapter {
    private Context mContext;
    private int mIconSize;

    public MenuAdapter(Context c, int iconSize) {
        mContext = c;
        mIconSize = iconSize;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(mIconSize, mIconSize));
            //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {R.drawable.menu_lift_status, R.drawable.menu_snow_report, 
    							   R.drawable.menu_weather, R.drawable.menu_trail_map, 
    							   R.drawable.menu_road_conditions, R.drawable.menu_emergency, 
    							   R.drawable.mammoth, R.drawable.menu_about};
}