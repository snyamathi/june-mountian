package com.snyamathi.June;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class Weather extends Activity {	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);       
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://forecast.weather.gov/MapClick.php?lat=37.7945928242851&lon=-119.04647827148437&site=rev&smap=1&marine=0&unit=0&lg=en"));
        startActivity(i);
        finish();
    }
}