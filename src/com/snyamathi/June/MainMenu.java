package com.snyamathi.June;

import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class MainMenu extends Activity {
	
	public static final String MY_MAMMOTH = "http://www.junemountain.com/jt/";
	public static Document sDocument = null;
	public static int sScreenWidth;
	
	Class<?>[] classes = {LiftStatus.class, SnowReport.class, Weather.class, TrailMap.class, 
								RoadConditions.class, Emergency.class, Mammoth.class, About.class};
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		
	    Thread t = new Thread() {
	    	public void run() {
	    		parseData();
	    	}
	    };
	    t.start();
		
		this.setTitle(R.string.main_menu);
		
		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
		sScreenWidth = display.getWidth(); 
		
	    GridView gridview = (GridView) findViewById(R.id.gridview);
	    gridview.setAdapter(new MenuAdapter(this, sScreenWidth/3));
	    
	    gridview.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	        	Intent intent = new Intent(MainMenu.this, classes[position]);
	        	startActivity(intent);
	        }
	    });
        
        try {
    	    PackageInfo pInfo;
            SharedPreferences prefs = getPreferences(0);
			pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
			if (prefs.getLong("lastRunVersionCode", 0) < pInfo.versionCode) {
				Editor editor = prefs.edit();
				editor.putLong("lastRunVersionCode", pInfo.versionCode);
				editor.commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Document parseData() {
		if (sDocument == null) {
			try {
				sDocument = Jsoup.parse(new URL(MY_MAMMOTH), 5000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sDocument;
	}	
}
