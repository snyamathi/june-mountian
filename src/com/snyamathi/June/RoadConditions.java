package com.snyamathi.June;

import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.widget.ImageView;

public class RoadConditions extends Activity {
	
	private boolean errorOccurred = false;
	String roadConditions = null;
	private ProgressDialog dialog;
    final Handler mHandler = new Handler();
    
    final Runnable mUpdateResults = new Runnable() {
    	public void run() {
    		if (errorOccurred) {
    			ImageView iv = new ImageView(RoadConditions.this);
    			iv.setImageResource(R.drawable.network_error);
    			iv.setBackgroundResource(R.drawable.gray_gradient);
    			setContentView(iv);
    		} else {
    			WebView webview = (WebView) findViewById(R.id.webview);
    			webview.loadData(roadConditions, "text/html", "utf-8");
    		}
        	dialog.cancel();
    	}
    };
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        
        Thread t = new Thread() {
        	public void run() {
        		try {
        			Document document = Jsoup.parse(new URL("http://www.mammothmountain.com/MyMammoth/"), 5000);
	        		roadConditions = getRoadConditions(document);
        		} catch (Exception e) {
        			errorOccurred = true;
        		} finally {
        			mHandler.post(mUpdateResults);
        		}
        	}
        };
        dialog = ProgressDialog.show(RoadConditions.this, "", "Loading. Please wait...", true);
        
        try {
        	t.start();
        } catch (NullPointerException e) {
        	t.stop();
        	t.start();
        }
    }
    
    public String getRoadConditions(Document document) throws NullPointerException {
    	Element e = document.getElementById("roadConditions");
    	return e.toString();
    }
}