package com.snyamathi.June;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

public class SnowReport extends Activity {
	
	private boolean errorOccurred = false;
	private ProgressDialog dialog;
	private String snowReport;
    final Handler mHandler = new Handler();
    
    final Runnable mUpdateResults = new Runnable() {
    	public void run() {
    		if (errorOccurred) {
    			ImageView iv = new ImageView(SnowReport.this);
    			iv.setImageResource(R.drawable.network_error);
    			iv.setBackgroundResource(R.drawable.gray_gradient);
    			setContentView(iv);
    		} else {
    				WebView webview = (WebView) findViewById(R.id.webview);
    	        	WebSettings settings = webview.getSettings();
    	        	settings.setDefaultTextEncodingName("utf-8");
    	        	webview.loadData(snowReport, "text/html", "utf-8");
    		}
        	dialog.cancel();
    	}
    };
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        
        Thread t = new Thread() {
        	public void run() {
        		try {
        			getSnowReport(MainMenu.parseData());
        		} catch (Exception e) {
        			errorOccurred = true;
        		} finally {
        			mHandler.post(mUpdateResults);
        		}
        	}
        };
        dialog = ProgressDialog.show(SnowReport.this, "", "Loading. Please wait...", true);
        t.start();
    }
    
    public void getSnowReport(Document document) {
    	Elements elements = document.getElementsByTag("h1");
    	for (Element e : elements) {
    		if (e.text().equals("Snow Report")) {
    			Elements siblings = e.siblingElements();
    			siblings.remove(1);
    			siblings.remove(4);
    			siblings.size();
    			for (int i = siblings.size()-1 ; i > 4 ; i--) {
    				siblings.remove(i);
    			}
    			snowReport = siblings.toString();
    	    	return;
    		}
    	}
    }
}