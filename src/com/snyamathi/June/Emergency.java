package com.snyamathi.June;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Emergency extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency);
        
    }
    
    public void openTreeWellPage(View v) {
    	Intent i = new Intent(Intent.ACTION_VIEW, 
    		       Uri.parse("http://www.treewelldeepsnowsafety.com"));
    	startActivity(i);
    }
}