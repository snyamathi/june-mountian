package com.snyamathi.June;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class About extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        
    }
    
    public void theAuthor(View v) {
    	Intent i = new Intent(About.this, Author.class);
    	startActivity(i);
    }
    
    public void launchPaypal(View v) {
    	Intent i = new Intent(Intent.ACTION_VIEW, 
 		       Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZJA72HFW2JP92"));
    	startActivity(i);
    }
    
}
