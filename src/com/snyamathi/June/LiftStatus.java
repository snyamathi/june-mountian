package com.snyamathi.June;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LiftStatus extends Activity {
	
	private boolean errorOccurred = false;
	private ImageView[] images = new ImageView[10];
	private char[] mStatus;
	
    final Handler mHandler = new Handler();
    
    final Runnable mUpdateResults = new Runnable() {
    	public void run() {
    		ProgressBar pb = (ProgressBar)findViewById(R.id.progress1);
    		pb.setVisibility(View.GONE);
    		pb = (ProgressBar)findViewById(R.id.progress2);
    		pb.setVisibility(View.GONE);
    		if (errorOccurred) {
    			Toast.makeText(LiftStatus.this, "Network error", Toast.LENGTH_LONG).show();
    		} else {
    			updateResultsInUi();
    		}
    	}
    };
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lift_status);
        
        images[0] = (ImageView)findViewById(R.id.imgJ1); 
		images[1] = (ImageView)findViewById(R.id.imgJ2); 
        images[2] = (ImageView)findViewById(R.id.imgJ3); 
        images[3] = (ImageView)findViewById(R.id.imgJ4); 
        images[4] = (ImageView)findViewById(R.id.imgJ6); 
        images[5] = (ImageView)findViewById(R.id.imgJ7); 
        images[6] = (ImageView)findViewById(R.id.imgP1); 
        images[7] = (ImageView)findViewById(R.id.imgP2); 
        images[8] = (ImageView)findViewById(R.id.imgP3); 
        images[9] = (ImageView)findViewById(R.id.imgP4);
        
        startLongRunningOperation();
    }
    
    protected void startLongRunningOperation() {
        Thread t = new Thread() {
        	public void run() {
        		try {
        			getLiftStatus(MainMenu.parseData());
        		} catch (Exception e) {
        			errorOccurred = true;
        		} finally {
        			mHandler.post(mUpdateResults);
        		}        		
        	}
        	
        };
        t.start();
    }
    
    private void getLiftStatus(Document document) {
    	mStatus = new char[10];
    	Elements liftElements = document.getElementsByAttributeValueStarting("src", "../snowreport/images/");
    	for (int i = 0 ; i < 10 ; i++){
    		String temp = liftElements.get(i).toString();
    		int first_char = temp.indexOf("alt=", 0) + 5;
    		mStatus[i] = temp.charAt(first_char);
    	}
    }
    
	protected void updateResultsInUi() {
		for (int i = 0; i<10; i++) {
			switch (mStatus[i]) {
			case 67:
				images[i].setImageResource(R.drawable.status_closed);
				images[i].setVisibility(View.VISIBLE);
				break;
			case 79:
				images[i].setImageResource(R.drawable.status_open);
				images[i].setVisibility(View.VISIBLE);					
				break;
			case 69:
				images[i].setImageResource(R.drawable.status_expected);
				images[i].setVisibility(View.VISIBLE);					
				break;
			case 48:
			case 87:
				images[i].setImageResource(R.drawable.status_weather_hold);
				images[i].setVisibility(View.VISIBLE);					
				break;
			}
        }
	}
}