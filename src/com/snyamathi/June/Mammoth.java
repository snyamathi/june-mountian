package com.snyamathi.June;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;

public class Mammoth extends Activity {
	
	Intent i;
	
	private boolean isCallable(Intent intent) {  
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent,   
            PackageManager.MATCH_DEFAULT_ONLY);  
        return list.size() > 0;  
}  
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		
		i = new Intent(Intent.ACTION_MAIN);
		i.setComponent(new ComponentName("com.snyamathi.Mammoth", "com.snyamathi.Mammoth.MainMenu"));
		
		if (isCallable(i)) {
			startActivity(i);
			finish();
		} else {
        	AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        	alertbox.setMessage("Mammoth app not found.  Would you like to get it now?");
        	alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {  		 
                // do something when the button is clicked
                public void onClick(DialogInterface arg0, int arg1) {
        			i = new Intent(Intent.ACTION_VIEW);
        			i.setData(Uri.parse("market://details?id=com.snyamathi.Mammoth"));
        			startActivity(i);
        			finish();
                }
            });
        	alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					finish();				
				}
			});
        	alertbox.show();
		}
	}
}
